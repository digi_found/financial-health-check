<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model {

    protected $table    = 'answers';
    protected $fillable = ['text', 'score'];

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */
    public function question() {
        return $this->belongsTo(\App\Question::class, 'question_id');
    }

}
