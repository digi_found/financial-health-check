<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    protected $table    = 'categories';
    protected $fillable = ['name'];

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */
    public function questions() {
        return $this->hasMany(\App\Question::class, 'category_id');
    }

}
