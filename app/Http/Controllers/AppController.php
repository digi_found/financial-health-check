<?php

namespace App\Http\Controllers;

use App\Http\Requests;

class AppController extends Controller {

    public function index() {
        return view('app');
    }

    public function pages($route = 'dashboard') {
        $view = "pages.$route";
        if (!view()->exists($view))
            abort(404);
        return view($view);
    }

}