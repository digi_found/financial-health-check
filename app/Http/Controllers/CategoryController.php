<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use App\Http\Requests\CategoryFormRequest;

class CategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = Category::all();
        return response()->return(compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryFormRequest $request) {
        $data = Category::create($request->all());
        return response()->return(compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Category::findOrFail($id);
        return response()->return(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryFormRequest $request, $id) {
        $model = Category::findOrFail($id);
        $model->update($request->all());
        $data = $model->fresh();
        return response()->return(compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $data = Category::findOrFail($id);
        $data->questions()->delete();
        $data->delete();
        return response()->return(compact('data'));
    }

}
