<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileFormRequest;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller {

    protected $user;

    public function __construct() {
        $this->user = auth()->user();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = $this->user;
        return response()->return(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileFormRequest $request, $id) {
        $model = $this->user;
        $model->update($request->all());
        $data = $model->fresh();
        return response()->return(compact('data'));
    }

    /**
     * @param $attribute string
     * @param $value string
     * @return bool true if validation pass, false if fail
     */
    public function validateCurrentPassword($attribute, $value) {
        return Hash::check($value, $this->user->password);
    }

}
