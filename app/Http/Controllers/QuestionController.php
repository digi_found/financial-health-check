<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\QuestionFormRequest;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = Question::sortByIndex()->get();
        return response()->return(compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionFormRequest $request) {
        $model = Question::create($request->all());
        $model->syncAnswers($request->get('answers', []));
        $data = $model->fresh();
        return response()->return(compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Question::findOrFail($id);
        return response()->return(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionFormRequest $request, $id) {
        $model = Question::findOrFail($id);
        $model->update($request->all());
        $model->syncAnswers($request->get('answers', []));
        $data = $model->fresh();
        return response()->return(compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $data = Question::findOrFail($id);
        $data->delete();
        return response()->return(compact('data'));
    }

    /**
     * Set custom sort to questions
     *
     * @param Request $request
     */
    public function sort(Request $request, $id) {
        $model = Question::findOrFail($id);
        DB::transaction(function () use ($request, $model) {
            $fromCategory = (int) $model->category_id;
            $toCategory   = (int) $request->get('category_id');
            $fromIndex    = (int) $model->index;
            $toIndex      = (int) ($fromIndex < $request->get('index') && $fromCategory === $toCategory ? ($request->get('index') + 1) : $request->get('index'));

            Question::where('category_id', $toCategory)->where('index', '>=', $toIndex)->sortByIndex('desc')->increment('index');
            $model->move($toCategory, $toIndex);
            Question::where('category_id', $fromCategory)->where('index', '>', $fromIndex)->sortByIndex('asc')->decrement('index');
        });
        $data = $model->fresh();
        return response()->return(compact('data'));
    }

}
