<?php

namespace App\Http\Requests;

class CategoryFormRequest extends Request {

    public function rules() {
        return [
            'name' => 'required|string|min:3|max:255'
        ];
    }

}
