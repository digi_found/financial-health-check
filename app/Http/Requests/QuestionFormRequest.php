<?php

namespace App\Http\Requests;

class QuestionFormRequest extends Request {

    public function rules() {
        return [
            'category_id'     => 'required|exists:categories,id',
            'text'            => 'required|string|min:5|max:500',
            'active'          => 'required|boolean',
            'answers.*.text'  => 'required|string|min:5|max:1000',
            'answers.*.score' => 'required|integer|min:1|max:255'
        ];
    }

    public function attributes() {
        $attributes = ['category_id' => 'category'];
        foreach ($this->request->get('answers', []) as $key => $value) {
            $attributes["answers.$key.text"]  = 'answer';
            $attributes["answers.$key.score"] = 'score';
        }
        return $attributes;
    }

}
