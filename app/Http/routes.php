<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => ['web']], function () {
    Route::get('/login', 'Auth\AuthController@getLogin');
    Route::post('/login', 'Auth\AuthController@postLogin');
    Route::get('/logout', 'Auth\AuthController@logout');

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/', 'AppController@index');
        Route::get('/pages/{ruta?}', 'AppController@pages');

        Route::resource('/categories', 'CategoryController');

        Route::put('/questions/sort/{id}', 'QuestionController@sort');
        Route::resource('/questions', 'QuestionController');

        Route::resource('/profile', 'ProfileController');
    });
});
