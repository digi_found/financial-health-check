<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider {

    public function boot() {
        response()->macro('return', function (array $params = [], $numerror = 200) {
            $args   = collect($params);
            $msg    = $args->get('msg', '');
            $data   = $args->get('data', []);
            $total  = (int) $args->get('total', count($data));
            $errors = (array) $args->get('errors', []);
            return response()->json(compact('msg', 'data', 'total', 'errors'), $numerror);
        });
    }

    public function register() {
        //
    }

}