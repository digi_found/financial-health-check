<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {

    protected $table    = 'questions';
    protected $fillable = ['category_id', 'text', 'active'];
    protected $casts    = ['active' => 'boolean'];
    protected $with     = ['answers'];
    //protected $hidden   = ['index'];

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */
    public function category() {
        return $this->belongsTo(\App\Category::class, 'category_id');
    }

    public function answers() {
        return $this->hasMany(\App\Answer::class, 'question_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */
    public function scopeSortByIndex($query, $direction = 'asc') {
        return $query->orderBy('index', $direction);
    }

    /*
    |--------------------------------------------------------------------------
    | Functions
    |--------------------------------------------------------------------------
    */
    public function syncAnswers($answers = []) {
        if (is_array($answers)) {
            $this->answers()->whereNotIn('id', array_pluck($answers, 'id'))->delete();
            foreach ($answers as $answer)
                $this->answers()->findOrNew(array_get($answer, 'id', 0))->fill($answer)->save();
        }
    }

    public function move($category_id, $index) {
        $this->category_id = $category_id;
        $this->index       = $index;
        $this->save();
    }

}
