<?php

use App\Question;
use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        foreach (range(1, 5) as $category_id)
            foreach (range(1, rand(3, 10)) as $index => $n)
                Question::create(['index' => $index, 'category_id' => $category_id, 'text' => "Question $n", 'active' => rand(0, 1)]);
    }

}
