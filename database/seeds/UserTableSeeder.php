<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        User::create([
            'level'    => 1,
            'name'     => 'Administrator',
            'email'    => 'admin@example.com',
            'password' => Hash::make('123456')
        ]);
    }
}
