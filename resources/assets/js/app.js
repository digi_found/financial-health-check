(function() {
    'use strict';
    angular
        .module('App', ['ui.router', 'ngResource', 'ngAnimate', 'angular.filter', 'ui.bootstrap', 'ui.sortable'])
        .config(['$httpProvider', '$urlRouterProvider', '$stateProvider', config]);

    function config($httpProvider, $urlRouterProvider, $stateProvider) {
        //$httpProvider.interceptors.push('InterceptorService');
        $urlRouterProvider.otherwise('/dashboard');
        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'pages/dashboard'
            })
            .state('questions', {
                url: '/questions',
                templateUrl: 'pages/questions.index',
                controller: 'IndexQuestionController',
                controllerAs: 'vm'
            })
            .state('profile', {
                url: '/profile',
                templateUrl: 'pages/users.profile',
                controller: 'ProfileController',
                controllerAs: 'vm'
            });
    }
}());