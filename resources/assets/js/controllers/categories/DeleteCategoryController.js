(function() {
    'use strict';
    angular.module('App').controller('DeleteCategoryController', ['$uibModalInstance', 'model', DeleteCategoryController]);
    function DeleteCategoryController(dialog, model) {
        var vm = this;
        vm.model = model;

        vm.delete = function() {
            vm.errors = [];
            //vm.isLoading = true;
            vm.model.$remove()
                .then(function(data) {
                    dialog.close(data);
                })
                .catch(function(response) {
                    vm.errors = angular.fromJson(response.data).errors;
                })
                .finally(function() {
                    //vm.isLoading = false;
                });
        };

        vm.cancel = function() {
            dialog.dismiss();
        };
    }
})();