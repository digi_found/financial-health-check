(function() {
    'use strict';
    angular.module('App').controller('SaveCategoryController', ['$uibModalInstance', 'DataService', 'model', SaveCategoryController]);
    function SaveCategoryController(dialog, data, model) {
        var vm = this;
        vm.model = model;

        vm.save = function() {
            vm.errors = {};
            //vm.isLoading = true;
            data.save('categories', vm.model)
                .then(function(data) {
                    dialog.close(data);
                })
                .catch(function(response) {
                    vm.errors = angular.fromJson(response.data).errors;
                })
                .finally(function() {
                    //vm.isLoading = false;
                });
        };

        vm.cancel = function() {
            dialog.dismiss();
        };
    }
})();