(function() {
    'use strict';
    angular.module('App').controller('DeleteQuestionController', ['$uibModalInstance', 'model', DeleteQuestionController]);
    function DeleteQuestionController(dialog, model) {
        var vm = this;
        vm.model = model;

        vm.delete = function() {
            vm.errors = [];
            //vm.isLoading = true;
            vm.model.$remove()
                .then(function(data) {
                    dialog.close(data);
                })
                .catch(function(response) {
                    vm.errors = angular.fromJson(response.data).errors;
                })
                .finally(function() {
                    //vm.isLoading = false;
                });
        };

        vm.cancel = function() {
            dialog.dismiss();
        };
    }
})();