(function() {
    'use strict';
    angular.module('App').controller('IndexQuestionController', ['DialogService', 'DataService', '$q', '$filter', IndexQuestionController]);
    function IndexQuestionController(dialog, data, $q, $filter) {
        var vm         = this,
            categories = [],
            questions  = [];

        /*
         |--------------------------------------------------------------------------
         | Get data
         |--------------------------------------------------------------------------
         */
        $q.all([
            data.list('categories').then(function(data) {
                categories = data;
            }),
            data.list('questions').then(function(data) {
                questions = $filter('groupBy')(data, 'category_id');
            })
        ]).then(function() {
            vm.data = categories;
            angular.forEach(vm.data, function(category) {
                category.questions = questions[category.id] ? questions[category.id] : [];
            });
        });

        /*
         |--------------------------------------------------------------------------
         | Category
         |--------------------------------------------------------------------------
         */
        vm.createCategory = function() {
            dialog.show('categories.save', 'SaveCategoryController', {model: {}}).then(function(data) {
                vm.data.push(data);
            });
        };
        vm.editCategory = function(model) {
            var backup = angular.copy(model);
            dialog.show('categories.save', 'SaveCategoryController', {model: model}).then(function(data) {
                angular.copy(data, model);
            }, function() {
                angular.copy(backup, model);
            });
        };
        vm.deleteCategory = function(model) {
            dialog.show('categories.delete', 'DeleteCategoryController', {model: model}).then(function() {
                angular.copy($filter('removeWith')(vm.data, {id: model.id}), vm.data);
            });
        };

        /*
         |--------------------------------------------------------------------------
         | Questions
         |--------------------------------------------------------------------------
         */
        vm.createQuestion = function(category) {
            dialog.show('questions.save', 'SaveQuestionController', {model: {category_id: category.id, active: true, answers: []}, category: category}).then(function(model) {
                if (!angular.isArray(category.questions))
                    category.questions = [];
                category.questions.push(model);
            });
        };
        vm.editQuestion = function(category, model) {
            var backup = angular.copy(model);
            dialog.show('questions.save', 'SaveQuestionController', {model: model, category: category}).then(function(data) {
                angular.copy(data, model);
            }, function() {
                angular.copy(backup, model);
            });
        };
        vm.deleteQuestion = function(models, model) {
            dialog.show('questions.delete', 'DeleteQuestionController', {model: model}).then(function() {
                angular.copy($filter('removeWith')(models, {id: model.id}), models);
            });
        };

        /*
         |--------------------------------------------------------------------------
         | Sortable
         |--------------------------------------------------------------------------
         */
        var sort = function(model, fromModel, toModel, fromCategoryId, toCategoryId, fromIndex, toIndex) {
            data.update('questions/sort', {
                    category_id: toCategoryId,
                    index: toIndex,
                    id: model.id
                })
                .then(function(data) {
                    angular.copy(data, model);
                }, function() {
                    toModel.splice(toIndex, 1);
                    fromModel.splice(fromIndex, 0, model);
                });
        };

        vm.sortableOptions = {
            axis: 'y',
            cursor: 'move',
            tolerance: 'pointer',
            connectWith: '.category-questions',
            helper: function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            },
            handle: function() {
                $('.sortable').sortable({handle: '.handle'});
            },
            stop: function(e, ui) {
                var model          = ui.item.sortable.model,
                    fromModel      = ui.item.sortable.sourceModel,
                    toModel        = ui.item.sortable.droptargetModel,
                    fromCategoryId = ui.item.sortable.source[0].attributes['category-id'].textContent,
                    toCategoryId   = ui.item.sortable.droptarget[0].attributes['category-id'].textContent,
                    fromIndex      = ui.item.sortable.index,
                    toIndex        = ui.item.sortable.dropindex;
                sort(model, fromModel, toModel, fromCategoryId, toCategoryId, fromIndex, toIndex);
            }
        };

    }
})();