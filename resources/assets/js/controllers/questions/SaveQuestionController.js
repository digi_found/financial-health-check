(function() {
    'use strict';
    angular.module('App').controller('SaveQuestionController', ['$uibModalInstance', 'DataService', 'model', 'category', SaveQuestionController]);
    function SaveQuestionController(dialog, data, model, category) {
        var vm = this;
        vm.model = model;
        vm.category = category;

        vm.addAnswer = function() {
            if (vm.model.answers.length < 5) {
                if (!angular.isArray(vm.model.answers))
                    vm.model.answers = [];
                vm.model.answers.push({});
            }
        };

        vm.deleteAnswer = function(answer) {
            var index = vm.model.answers.indexOf(answer);
            if (index >= 0)
                vm.model.answers.splice(index, 1);
        };

        vm.save = function() {
            vm.errors = {};
            //vm.isLoading = true;
            data.save('questions', vm.model)
                .then(function(data) {
                    dialog.close(data);
                })
                .catch(function(response) {
                    vm.errors = angular.fromJson(response.data).errors;
                })
                .finally(function() {
                    //vm.isLoading = false;
                });
        };

        vm.cancel = function() {
            dialog.dismiss();
        };
    }
})();