(function() {
    'use strict';
    angular.module('App').controller('ProfileController', ['DataService', ProfileController]);
    function ProfileController(data) {
        var vm = this;
        //vm.isLoading = true;

        data.get('profile').then(function(profile) {
            vm.model = profile;
        }).catch(function() {
            vm.model = {};
        }).finally(function() {
            //vm.isLoading = false;
        });

        vm.save = function() {
            vm.errors = [];
            //vm.isLoading = true;
            vm.model.$update()
                .then(function() {
                    //$scope.$parent.vm.showToast();
                })
                .catch(function(response) {
                    vm.errors = angular.fromJson(response.data).errors;
                })
                .finally(function() {
                    //vm.isLoading = false;
                });
        };
    }
})();