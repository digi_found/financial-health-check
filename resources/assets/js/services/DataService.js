(function() {
    'use strict';
    angular.module('App').factory('DataService', ['$resource', DataService]);
    function DataService($resource) {
        var getData = function(response, status, isArray) {
            if (status === 200) {
                var data = angular.fromJson(response).data;
                return isArray ? data : (angular.isArray(data) ? {} : data);
            }
            return response;
        };
        var getResource = function(url) {
            return $resource(url + "/:id", {
                id: "@id"
            }, {
                query: {
                    method: "GET",
                    isArray: true,
                    transformResponse: function(data, headers, status) {
                        return getData(data, status, true);
                    }
                },
                get: {
                    method: "GET",
                    transformResponse: function(data, headers, status) {
                        return getData(data, status);
                    }
                },
                save: {
                    method: "POST",
                    transformResponse: function(data, headers, status) {
                        return getData(data, status);
                    }
                },
                update: {
                    method: "PUT",
                    transformResponse: function(data, headers, status) {
                        return getData(data, status);
                    }
                },
                remove: {
                    method: "DELETE",
                    transformResponse: function(data, headers, status) {
                        return getData(data, status);
                    }
                }
            });
        };
        return {
            list: function(url, query) {
                return getResource(url).query(query).$promise;
            },
            get: function(url, query) {
                return getResource(url).get(query).$promise;
            },
            create: function(url, model) {
                return getResource(url).save(model).$promise;
            },
            update: function(url, model) {
                return getResource(url).update(model).$promise;
            },
            save: function(url, model) {
                return (model.id && model.id > 0) ? this.update(url, model) : this.create(url, model);
            },
            delete: function(url, model) {
                return getResource(url).remove(model).$promise;
            }
        };
    }
})();