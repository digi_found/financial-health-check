(function() {
    'use strict';
    angular.module('App').service('DialogService', ['$uibModal', DialogService]);
    function DialogService(dialog) {
        return {
            show: function(tpl, controller, locals) {
                return dialog.open({
                    animation: true,
                    templateUrl: 'pages/' + tpl,
                    controller: controller,
                    controllerAs: 'vm',
                    resolve: locals
                }).result;
            }
        };
    }
})();
