(function() {
    'use strict';
    angular.module('App').factory('InterceptorService', ['$q', '$injector', InterceptorService]);
    function InterceptorService($q, $injector) {
        return {
            responseError: function(response) {
                var DialogService = $injector.get('DialogService');
                if (response.status == 401) {
                    var $rootScope = $injector.get('$rootScope'),
                        $state     = $injector.get('$state');
                    DialogService.alert('Session expired', "You don't have access to this resource. You must login first.", 'Login').then(function() {
                        $rootScope.session = false;
                        $state.transitionTo('login');
                    });
                } else if ([422, 404].indexOf(response.status) < 0) {
                    DialogService.alert('Error', response.data.errors, 'Got it!');
                }
                return $q.reject(response);
            }
        };
    }
})();