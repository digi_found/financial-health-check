<!DOCTYPE html>
<html lang="en" ng-app="App">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin Panel</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
        <link href="{{ elixir('assets/css/all.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-fixed-top">
                @include('includes.header')
                @include('includes.menu')
            </nav>
            <div id="page-wrapper">
                <div class="container-fluid" ui-view></div>
            </div>
        </div>
        <script type="text/ng-template"></script>
        <script src="{{ elixir('assets/js/all.js') }}"></script>
    </body>
</html>