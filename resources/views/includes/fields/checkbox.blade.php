<div class="checkbox">
    <label>
        <input type="checkbox" {!! $attributes !!}>
        {!! $label !!}
    </label>
</div>