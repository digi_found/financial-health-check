@if(!empty($text))
    <label @if(!empty($name)) for="{!! $name !!}" @endif class="control-label"><strong>{!! $text !!}</strong></label>
@endif