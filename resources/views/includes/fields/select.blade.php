<div class="form-group" {!! "ng-class=\"{'has-error': vm.errors['$name']}\"" !!}>
    @include('includes.fields.label', ['text' => $label, 'name' => $name])
    <div class="controls">
        <select {!! $attributes !!} class="form-control"></select>
        @include('includes.fields.errors', ['name' => $name])
    </div>
</div>