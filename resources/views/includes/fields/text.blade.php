<div class="form-group" {!! "ng-class=\"{'has-error': vm.errors['$name']}\"" !!}>
    @include('includes.fields.label', ['text' => $label, 'name' => $name])
    <div class="controls">
        <input type="text" name="{!! $name !!}" {!! $attributes !!} class="form-control">
        @include('includes.fields.errors', ['name' => $name])
    </div>
</div>