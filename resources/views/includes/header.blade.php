<div class="container-fluid">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" ng-init="navCollapsed = true" ng-click="navCollapsed = !navCollapsed">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ url('/') }}">Admin Panel</a>
    </div>
    <div class="navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
            <li uib-dropdown>
                <a href uib-dropdown-toggle><i class="fa fa-user"></i> {{ auth()->user()->name }} <b class="caret"></b></a>
                <ul uib-dropdown-menu aria-labelledby="user-menu">
                    <li ui-sref-active="active">
                        <a ui-sref="profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>