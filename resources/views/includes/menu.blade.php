<div class="collapse navbar-collapse" uib-collapse="navCollapsed">
    <ul class="nav navbar-nav side-nav">
        <li ui-sref-active="active">
            <a ui-sref="dashboard" ng-click="navCollapsed = !navCollapsed"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
        </li>
        <li ui-sref-active="active">
            <a ui-sref="questions" ng-click="navCollapsed = !navCollapsed"><i class="fa fa-fw fa-question-circle"></i> Questions</a>
        </li>
    </ul>
</div>