<div class="modal-header">
    <h4>@yield('title')</h4>
</div>
<div class="modal-body">
    @yield('body')
</div>
<div class="modal-footer">
    @yield('footer')
</div>