@extends('layouts.dialog')

@section('title')
    Delete Question
@endsection

@section('body')
    Are you sure you want to delete the selected category?
    <p><strong class="text-danger">*All questions related to this category will be deleted</strong></p>
@endsection

@section('footer')
    <button class="btn btn-danger" type="button" ng-click="vm.delete()">Delete</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancel()">Cancel</button>
@endsection