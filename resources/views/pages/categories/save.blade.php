@extends('layouts.dialog')

@section('title')
    @{{ vm.model.id > 0 ? 'Edit' : 'Add' }} Category
@endsection

@section('body')
    <form>
        @include('includes.fields.text', ['label' => 'Name', 'name' => 'name', 'attributes' => 'ng-model="vm.model.name" autofocus'])
    </form>
@endsection

@section('footer')
    <button class="btn btn-primary" type="button" ng-click="vm.save()">Save</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancel()">Cancel</button>
@endsection