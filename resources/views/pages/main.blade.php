<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        @include('includes.header')
        @include('includes.menu')
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div ui-view></div>
        </div>
    </div>
</div>