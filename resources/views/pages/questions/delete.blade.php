@extends('layouts.dialog')

@section('title')
    Delete Question
@endsection

@section('body')
    Are you sure you want to delete the selected question?
@endsection

@section('footer')
    <button class="btn btn-danger" type="button" ng-click="vm.delete()">Delete</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancel()">Cancel</button>
@endsection