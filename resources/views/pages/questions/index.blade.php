<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Questions</h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> <a ui-sref="dashboard">Dashboard</a>
            </li>
            <li class="active">
                <i class="fa fa-question-circle"></i> Questions
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 text-right">
        <button class="btn btn-sm btn-primary" type="button" tooltip-placement="left" uib-tooltip="Add Category" ng-click="vm.createCategory()"><i class="fa fa-plus"></i></button>
    </div>
    <div class="col-lg-12">
        <table class="table" ng-repeat="category in vm.data track by $index">
            <thead>
                <tr>
                    <th width="50px">@{{ $index < 1 ? 'N°' : '' }}</th>
                    <th width="50px">@{{ $index < 1 ? '' : '' }}</th>
                    <th>@{{ $index < 1 ? 'Question' : '' }}</th>
                    <th class="btn-tools-3"></th>
                    <th class="btn-tools-1"></th>
                </tr>
                <tr class="active">
                    <td colspan="3"><strong>@{{ category.name }}</strong></td>
                    <td align="right">
                        <button class="btn btn-xs btn-primary" type="button" uib-tooltip="Add question" ng-click="vm.createQuestion(category)"><i class="fa fa-plus"></i></button>
                        <button class="btn btn-xs btn-info" type="button" uib-tooltip="Edit" ng-click="vm.editCategory(category)"><i class="fa fa-edit"></i></button>
                        <button class="btn btn-xs btn-danger" type="button" uib-tooltip="Delete" ng-click="vm.deleteCategory(category)"><i class="fa fa-trash"></i></button>
                    </td>
                    <td align="center"></td>
                </tr>
            </thead>
            <tbody ui-sortable="vm.sortableOptions" ng-model="category.questions" category-id="@{{ category.id }}" class="sortable category-questions">
                <tr ng-if="!category.questions.length">
                    <td colspan="5">No records</td>
                </tr>
                <tr ng-repeat="question in category.questions track by $index">
                    <td align="center">@{{ $index + 1 }}</td>
                    <td align="center"><i class="fa fa-@{{ question.active ? 'check-circle text-success' : 'ban text-danger' }} fa-lg"></i></td>
                    <td>@{{ question.text }}</td>
                    <td align="right">
                        <button class="btn btn-xs btn-info" type="button" uib-tooltip="Edit" ng-click="vm.editQuestion(category, question)"><i class="fa fa-edit"></i></button>
                        <button class="btn btn-xs btn-danger" type="button" uib-tooltip="Delete" ng-click="vm.deleteQuestion(category.questions, question)"><i class="fa fa-trash"></i></button>
                    </td>
                    <td align="center"><i class="fa fa-bars handle"></i></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>