@extends('layouts.dialog')

@section('title')
    <div class="row">
        <div class="col-xs-6 text-left">@{{ vm.model.id > 0 ? 'Edit' : 'Add' }} Question</div>
        <div class="col-xs-6 text-right">
            <small>@{{ vm.category.name }}</small>
        </div>
    </div>
@endsection

@section('body')
    <form>
        <div class="row">
            <div class="col-xs-12">
                @include('includes.fields.text', ['label' => 'Type your question', 'name' => 'text', 'attributes' => 'ng-model="vm.model.text" autofocus'])
            </div>
        </div>

        <div class="row margin-top-15">
            <div class="col-xs-8">
                @include('includes.fields.label', ['text' => 'Type your answers'])
            </div>
            <div class="col-xs-3">
                @include('includes.fields.label', ['text' => 'Scores'])
            </div>
            <div class="col-xs-1">
                <button class="btn btn-xs btn-default pull-right" type="button" ng-click="vm.addAnswer()"><i class="fa fa-plus"></i></button>
            </div>
            <div class="col-xs-12 margin-top-10" ng-show="!vm.model.answers.length">
                <p class="text-muted">No answers yet...</p>
            </div>
        </div>

        <div class="row" ng-repeat="answer in vm.model.answers track by $index">
            <div class="col-xs-1 margin-top-15">
                @{{ $index + 1 }}.
            </div>
            <div class="col-xs-7">
                @include('includes.fields.text', ['label' => '', 'name' => 'answers.@{{ $index }}.text', 'attributes' => 'ng-model="answer.text"'])
            </div>
            <div class="col-xs-3">
                @include('includes.fields.text', ['label' => '', 'name' => 'answers.@{{ $index }}.score', 'attributes' => 'ng-model="answer.score"'])
            </div>
            <div class="col-xs-1">
                <button type="button" class="btn btn-xs btn-danger btn-field pull-right" ng-click="vm.deleteAnswer(answer)"><i class="fa fa-trash"></i></button>
            </div>
        </div>
    </form>
@endsection

@section('footer')
    <div class="row">
        <div class="col-xs-4 text-left">
            @include('includes.fields.checkbox', ['label' => 'Active', 'attributes' => 'ng-model="vm.model.active" value="1"'])
        </div>
        <div class="col-xs-8">
            <button class="btn btn-primary" type="button" ng-click="vm.save()">Save</button>
            <button class="btn btn-default" type="button" ng-click="vm.cancel()">Cancel</button>
        </div>
    </div>
@endsection