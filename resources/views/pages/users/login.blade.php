<!DOCTYPE html>
<html lang="en" class="login-page">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
        <link href="{{ elixir('assets/css/all.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body class="login-page">
        <div class="login-container">
            <div class="login-subcontainer">
                <div class="panel panel-default login-panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="{{ url('/login') }}">
                            <fieldset>
                                {!! csrf_field() !!}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input class="form-control" placeholder="Email" name="email" type="email" value="{{ old('email') }}" autofocus>
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="" autofocus>
                                </div>
                                @if($errors->has('email') || $errors->has('password'))
                                    <div class="form-group has-error">
                                        @if ($errors->has('email'))
                                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                        @endif
                                        @if ($errors->has('password'))
                                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                        @endif
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-xs-7">
                                        <div class="checkbox">
                                            <label>
                                                <input name="remember" type="checkbox" value="Remember Me"{{ old('remember') ? 'checked' : '' }}>Remember Me
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-5">
                                        <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-btn fa-sign-in"></i> Login</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>