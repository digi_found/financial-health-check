<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Profile</h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> <a ui-sref="dashboard">Dashboard</a>
            </li>
            <li class="active">
                <i class="fa fa-user"></i> Profile
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-6" style="max-width: 400px;">
        <form>
            @include('includes.fields.text', ['label' => 'Name', 'name' => 'name', 'attributes' => 'ng-model="vm.model.name"'])
            @include('includes.fields.email', ['label' => 'Email', 'name' => 'email', 'attributes' => 'ng-model="vm.model.email"'])
            @include('includes.fields.password', ['label' => 'Current Password', 'name' => 'password_current', 'attributes' => 'ng-model="vm.model.password_current"'])
            @include('includes.fields.password', ['label' => 'New Password', 'name' => 'password_new', 'attributes' => 'ng-model="vm.model.password_new"'])
            @include('includes.fields.password', ['label' => 'Confirm Password', 'name' => 'password_new_confirmation', 'attributes' => 'ng-model="vm.model.password_new_confirmation"'])
            <button class="btn btn-primary pull-right" type="button" ng-click="vm.save()">Save</button>
        </form>
    </div>
</div>